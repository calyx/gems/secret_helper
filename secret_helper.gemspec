Gem::Specification.new do |spec|
  spec.name        = "secret_helper"
  spec.version     = "0.1.0"
  spec.authors     = ["elijah"]
  spec.homepage    = "https://0xacab.org/calyx/gems/secret_helper"
  spec.summary     = "Useful methods for encrypted URLs, random codes, and encrypted tickets"
  spec.license     = "MIT"
  spec.files = Dir["{app,lib}/**/*", "LICENSE", "README.md"]

  spec.add_dependency('rbnacl')
end
